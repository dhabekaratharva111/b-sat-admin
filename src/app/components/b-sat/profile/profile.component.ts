import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  studentDetails: any;


  constructor(private fb: FormBuilder, private httpService : HttpService) { }

  ngOnInit() {
    this.studentDetails = this.httpService. getUserDetails().data
    console.log('this.studentDetails', this.studentDetails)
  }


  

}
