import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BSatRoutingModule } from './b-sat-routing.module';
import { ELearningComponent } from './e-learning/e-learning.component';
import { ProfileComponent } from './profile/profile.component';
import { StudyMaterialComponent } from './study-material/study-material.component';
import { ExamComponent } from './exam/exam.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ELearningComponent,
    ProfileComponent,
    StudyMaterialComponent,
    ExamComponent
  ],
  imports: [
    CommonModule,
    BSatRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class BSatModule { }
