import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { id } from 'date-fns/esm/locale';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-e-learning',
  templateUrl: './e-learning.component.html',
  styleUrls: ['./e-learning.component.scss']
})
export class ELearningComponent {
  newForm: FormGroup;
  subjects: any[] = [];
  getSubject: any=[];
  classes = [
    { key: '1', value: 'Class 1' },
    { key: '2', value: 'Class 2' },
    { key: '3', value: 'Class 3' },
    { key: '4', value: 'Class 4' },
    { key: '5', value: 'Class 5' },
    { key: '6', value: 'Class 6' },
    { key: '7', value: 'Class 7' },
    { key: '8', value: 'Class 8' },
    { key: '8', value: 'Class 8' },
    { key: '9', value: 'Class 9' },
    { key: '10', value: 'Class 10' },
    { key: '11', value: 'Class 11' },
    { key: '12', value: 'Class 12' },
 
  ];
  getChapter: any[];
  getYoutubeLinks: any[] = [];

  constructor(private fb: FormBuilder, private httpService : HttpService) { }

  ngOnInit() {
    this.newForm = this.fb.group({
      className: [null, Validators.required],
      subjectName: [null, Validators.required],
      chapterName: [null, Validators.required]
    });

    // Subscribe to changes in the class dropdown to load subjects dynamically
    this.newForm.get('className')!.valueChanges.subscribe((selectedClass) => {
      this.loadSubjectsForClass(selectedClass);
    });
    if (!this.getSubject) {
      this.getSubject = [];
    }
  }

  loadSubjectsForClass(selectedClassId: string) {
    this.httpService.getById('subject/getSubjectByClass', selectedClassId).subscribe(
      (response) => {
        this.getSubject = response.data ? [response.data] : [];
        console.log('this.getSubject', this.getSubject)
      },
      (error) => {
        console.error('API Error:', error);
      }
    );
  }

  loadChapterForSubject(selectedSubjectId: string) {
    if (selectedSubjectId) {
      this.httpService.getById('chapter/getChapterBySubject', selectedSubjectId).subscribe(
        (response) => {
          this.getChapter = Array.isArray(response.data) ? response.data : [response.data];
          console.log('this.getChapter', this.getChapter);
        },
        (error) => {
          console.error('API Error:', error);
        }
      );
    } else {
      this.getChapter = [];
    }
  }

  getVideoLink(): string {
    return this.getYoutubeLinks.length > 0 ? this.getYoutubeLinks[0].videoLink : '';
  }

  loadYoutubeLinks(selectedChapterId: string) {
    if (selectedChapterId) {
      this.httpService.getById('addElerning/getYoutubeVideoByChapterId', selectedChapterId).subscribe(
        (response) => {
          this.getYoutubeLinks = Array.isArray(response.data) ? response.data : [response.data];
          console.log('this.getYoutubeLinks', this.getYoutubeLinks);
        },
        (error) => {
          console.error('API Error:', error);
        }
      );
    } else {
      this.getYoutubeLinks = [];
    }
  }  
  
  changeSubject() {
    const selectedSubject: string | null = this.newForm.get('subjectName')?.value;
    console.log('Selected subject:', selectedSubject);
    if (selectedSubject) {
      this.loadChapterForSubject(selectedSubject);
    }
  }

  changeChapter() {
    const selectedChapter = this.newForm.get('chapterName')?.value;
    console.log('Selected chapter:', selectedChapter);
    this.loadYoutubeLinks(selectedChapter);
  }

  showMessage(): boolean {
    return this.newForm.invalid || !this.newForm.get('className')?.value ||
      !this.newForm.get('subjectName')?.value || !this.newForm.get('chapterName')?.value;
  }
}
