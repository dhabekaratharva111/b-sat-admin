import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ELearningComponent } from './e-learning/e-learning.component';
import { StudyMaterialComponent } from './study-material/study-material.component';
import { ExamComponent } from './exam/exam.component';
import { ProfileComponent } from './profile/profile.component';

var routingAnimation = localStorage.getItem('animate')

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'e-learning',
        component: ELearningComponent,
        data: {
          title: "E-learning",
          breadcrumb: "E-learning",
          animation: [routingAnimation]
        }
      },
      {
        path: 'study-material',
        component: StudyMaterialComponent,
        data: {
          title: "Study Material",
          breadcrumb: "Study Material",
          animation: [routingAnimation]
        }
      },
      {
        path: 'exam',
        component: ExamComponent,
        data: {
          title: "Exam",
          breadcrumb: "B-Sat Exam",
          animation: [routingAnimation]
        }
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {
          title: "Student Profile",
          breadcrumb: "Profile",
          animation: [routingAnimation]
        }
      },
    ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BSatRoutingModule { }
