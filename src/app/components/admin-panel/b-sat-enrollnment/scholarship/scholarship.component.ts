import { Component, QueryList, TemplateRef, ViewChildren } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { ProductListDirective, SortEvent } from 'src/app/shared/directives/product-list.directive';
import { productList } from 'src/app/shared/interface/product-list';
import { ProductListService } from 'src/app/shared/services/ecommerce/product-list.service';
import { HttpService } from 'src/app/shared/services/http/http.service';

const Swal = require('sweetalert2')

@Component({
  selector: 'app-scholarship',
  templateUrl: './scholarship.component.html',
  styleUrls: ['./scholarship.component.scss']
})
export class ScholarshipComponent {
  products$: Observable<productList[]>;
  total$: Observable<number>;

  // public PRODUCTLIST = data.PRODUCTLIST 

  @ViewChildren(ProductListDirective)
  headers!: QueryList<ProductListDirective>;
  response: any;
  subjectDetails: any;
  modalRef?: BsModalRef;
  template: TemplateRef<void>
  isModalOpen = false;
  selectedDataToView: any;

  constructor(public service: ProductListService ,private router: Router, private httpService: HttpService,private modalService: BsModalService) {
    this.products$ = service.support$;
    this.total$ = service.total$;

    this.response = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    console.log('this.qParams', this.response);

    this.getAllRegistrationForScholership();
  }

getAllRegistrationForScholership(){
  this.httpService.get('registrationForScholership/getAllRegistrationForScholership').subscribe(
    (res) => {
      this.subjectDetails = res.data;
      console.log('this.subjectDetails', this.subjectDetails)
    },
    (error) => {
      console.log('error', error);
    }
  )
}
  


deleteChapter(id : any) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });
  
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "Once deleted, you will not be able to recover data! ",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then((result: any) => {
      if (result.value) {
        // If the user confirms the deletion
        this.httpService.delete('registrationForScholership/deleteRegistrationForScholership',id).subscribe(
          (res) => {
            this.getAllRegistrationForScholership();
            swalWithBootstrapButtons.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
          },
          (error) => {
            console.log('error', error);
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // If the user cancels the deletion
      }
    });
  }

  editTheForm(item: any,) {
    item.isEditClick = true
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };
    this.router.navigate(['/admin-panel/bharat-sat/add-scholarship'], { state: objToSend });
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  // openModal(template: TemplateRef<void>) {
  //   this.modalRef = this.modalService.show(template);
  // }

  openModalToShowDetailsData(event: Event, item:any) {
    event.preventDefault();
    this.selectedDataToView = item?.studyMaterial;
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }

  getLimitedContent(content: string): string {
    const limit = 50; // Adjust the limit as needed
    if (content.length <= limit) {
      return content;
    }
    return content.substring(0, limit) + '...';
  }
}
