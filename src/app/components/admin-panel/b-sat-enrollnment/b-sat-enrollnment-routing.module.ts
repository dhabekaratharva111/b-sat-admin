import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionListComponent } from './question-list/question-list.component';
import { BSatEnrollnmentComponent } from './enrollnment/b-sat-enrollnment.component';
import { AddQuestionComponent } from './add-question/add-question.component';
import { AddTestComponent } from './add-test/add-test.component';
import { TestListComponent } from './test-list/test-list.component';
import { BsatExamResultComponent } from './bsat-exam-result/bsat-exam-result.component';
import { ScholarshipComponent } from './scholarship/scholarship.component';
import { AddScholarshipComponent } from './add-scholarship/add-scholarship.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'enrollnment',
        component: BSatEnrollnmentComponent,
        data: {
          title: "Enrollnment List",
          breadcrumb: "Enrollnment List",
        }
      },
      {
        path: 'question-list',
        component: QuestionListComponent,
        data: {
          title: "Question Set",
          breadcrumb: "Question Set",
        }
      },
      {
        path: 'add-question',
        component: AddQuestionComponent,
        data: {
          title: "Add Question",
          breadcrumb: "add-question",
        }
      },
      {
        path: 'add-test',
        component: AddTestComponent,
        data: {
          title: "Add Test",
          breadcrumb: "Test",
        }
      },
      {
        path: 'test-list',
        component: TestListComponent,
        data: {
          title: "Schedule Test",
          breadcrumb: "Schedule Test",
        }
      },
      {
        path: 'bsat-exam-result',
        component: BsatExamResultComponent,
        data: {
          title: "Exam Result",
          breadcrumb: "Exam Result",
        }
      },
      {
        path: 'scholarship',
        component: ScholarshipComponent,
        data: {
          title: "Scholarship",
          breadcrumb: "Scholarship",
        }
      },
      {
        path: 'add-scholarship',
        component: AddScholarshipComponent,
        data: {
          title: "Add Scholarship",
          breadcrumb: "Add Scholarship",
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BSatEnrollnmentRoutingModule { }
