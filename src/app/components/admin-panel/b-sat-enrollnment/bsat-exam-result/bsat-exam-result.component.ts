import { Component, QueryList, ViewChildren } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs';
import { ProductListDirective, SortEvent } from 'src/app/shared/directives/product-list.directive';
import { productList } from 'src/app/shared/interface/product-list';
import { ProductListService } from 'src/app/shared/services/ecommerce/product-list.service';
import { HttpService } from 'src/app/shared/services/http/http.service';

const Swal = require('sweetalert2')

@Component({
  selector: 'app-bsat-exam-result',
  templateUrl: './bsat-exam-result.component.html',
  styleUrls: ['./bsat-exam-result.component.scss']
})
export class BsatExamResultComponent {
  products$: Observable<productList[]>;
  total$: Observable<number>;

  // public PRODUCTLIST = data.PRODUCTLIST 

  @ViewChildren(ProductListDirective)
  headers!: QueryList<ProductListDirective>;
  response: any;
  userDetails: any;

  constructor(public service: ProductListService ,private router: Router, private httpService: HttpService) {
    this.products$ = service.support$;
    this.total$ = service.total$;

    this.response = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    console.log('this.qParams', this.response);

    this.getAllResult();
  }

getAllResult(){
  this.httpService.get('submitBsatExamTest/getAllSubmitBsatExamTest').subscribe(
    (res) => {
      this.userDetails = res.data;
      console.log('this.categoryDetails', this.userDetails)
     
    },
    (error) => {
      console.log('error', error);
    }
  )
}
  

  // deleteBibiyaneCategory(id : any) {
  //   const swalWithBootstrapButtons = Swal.mixin({
  //     customClass: {
  //       confirmButton: 'btn btn-success',
  //       cancelButton: 'btn btn-danger'
  //     },
  //     buttonsStyling: false,
  //   });
  
  //   swalWithBootstrapButtons.fire({
  //     title: 'Are you sure?',
  //     text: "Once deleted, you will not be able to recover data! ",
  //     type: 'warning',
  //     showCancelButton: true,
  //     confirmButtonText: 'OK',
  //     cancelButtonText: 'Cancel',
  //     reverseButtons: true
  //   }).then((result: any) => {
  //     if (result.value) {
  //       // If the user confirms the deletion
  //       this.httpService.delete('bibiyaneSubCategory/deleteBibiyaneSubCat',id).subscribe(
  //         (res) => {
  //           this.getAllUser();
  //           swalWithBootstrapButtons.fire(
  //             'Deleted!',
  //             'Your file has been deleted.',
  //             'success'
  //           );
  //         },
  //         (error) => {
  //           console.log('error', error);
  //         }
  //       );
  //     } else if (result.dismiss === Swal.DismissReason.cancel) {
  //       // If the user cancels the deletion
  //     }
  //   });
  // }

  // editTheForm(item: any,) {
  //   let objToSend: NavigationExtras = {
  //     queryParams: item,
  //     skipLocationChange: false,
  //     fragment: 'top'
  //   };
  //   this.router.navigate(['/admin-panel/user/edit-user'], { state: objToSend });
  // }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
