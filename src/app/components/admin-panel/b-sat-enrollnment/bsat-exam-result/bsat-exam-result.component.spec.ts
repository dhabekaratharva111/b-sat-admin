import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BsatExamResultComponent } from './bsat-exam-result.component';

describe('BsatExamResultComponent', () => {
  let component: BsatExamResultComponent;
  let fixture: ComponentFixture<BsatExamResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BsatExamResultComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BsatExamResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
