import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-scholarship',
  templateUrl: './add-scholarship.component.html',
  styleUrls: ['./add-scholarship.component.scss']
})
export class AddScholarshipComponent {
  studyMaterialFrom: FormGroup;
  subjectDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  catImageFile: File | null = null;
  chapterImageSrc: string | null = null;
  selectedId: any;
  receviedData: any;
  chapterImg: any;
  isEditClicked: any = false
  base_url: any = environment.apiEndpoint;
  chapterDetails: any;

  classArray = [
    { key: '1', value: 'Class 1', disabled: false },
    { key: '2', value: 'Class 2', disabled: false },
    { key: '3', value: 'Class 3', disabled: false },
    { key: '4', value: 'Class 4', disabled: false },
    { key: '5', value: 'Class 5', disabled: false },
    { key: '6', value: 'Class 6', disabled: false },
    { key: '7', value: 'Class 7', disabled: false },
    { key: '8', value: 'Class 8', disabled: false },
    { key: '8', value: 'Class 8', disabled: false },
    { key: '9', value: 'Class 9', disabled: false },
    { key: '10', value: 'Class 10', disabled: false },
    { key: '11', value: 'Class 11', disabled: false },
    { key: '12', value: 'Class 12', disabled: false },
  ]

  constructor(private httpService: HttpService, private fb: FormBuilder, private http: HttpClient, private router: Router,) {
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if (this.receviedData) {
      if (this.receviedData.isEditClick) {
        this.isEditClicked = this.receviedData.isEditClick
      } else {
        this.isEditClicked = false;

      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {
  }
  expertForm() {
    this.studyMaterialFrom = this.fb.group({
      classId: [this.receviedData ? this.receviedData.classId : '', Validators.required],
      medium: [this.receviedData ? this.receviedData.medium : '', Validators.required],
      examFee: [this.receviedData ? this.receviedData.examFee : '', Validators.required],
    });
  }

  changeClassId() {
    if (this.studyMaterialFrom) {
      const selectedclassId = this.studyMaterialFrom.get('classId')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }

  changeMediumId(){
    if (this.studyMaterialFrom) {
      const selectedmedium = this.studyMaterialFrom.get('medium')?.value;
      console.log('Selected Category ID:', selectedmedium);
    }
  }

  createRegistrationForScholership() {
    this.httpService.post("registrationForScholership/createRegistrationForScholership", this.studyMaterialFrom.value).subscribe({
      next: (response) => {
        this.subjectDetails = response.data;
        console.log('this.subjectDetails', this.subjectDetails);
        this.router.navigate(['/admin-panel/bharat-sat/scholarship']);
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  updateRegistrationForScholership() {
    this.httpService.put("registrationForScholership/updateRegistrationForScholership", this.receviedData._id, this.studyMaterialFrom.value).subscribe({
      next: (response) => {
        console.log('this.subjectDetails', this.subjectDetails);
        this.router.navigate(['/admin-panel/bharat-sat/scholarship']);
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  clear() {
    this.studyMaterialFrom.reset();
    this.chapterImageSrc = null; // Clear the image URL
  }
}
