import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss']
})
export class AddQuestionComponent {
  questionForm: FormGroup;
  TestDetails: any;
  receviedData: any;
  isEditClicked: boolean = false;

  constructor(
    private httpService: HttpService,
    private fb: FormBuilder,
    private http: HttpClient,
    private router: Router
  ) {
    this.receviedData =
      this.router.getCurrentNavigation()?.extras.state?.["queryParams"];
    if (this.receviedData) {
      if (this.receviedData.isEditClick) {
        this.isEditClicked = this.receviedData.isEditClick;
      } else {
        this.isEditClicked = false;
      }
    }
    console.log("this.receviedData", this.receviedData);
  }

  ngOnInit() {
    this.expertForm();
    this.getAllQuestions();
  }

  expertForm() {
    this.questionForm = this.fb.group({
      bsatExamTestId: [
        this.receviedData ? this.receviedData.bsatExamTestId._id : "",
        Validators.required,
      ],
      question: [
        this.receviedData ? this.receviedData.question : "",
        Validators.required,
      ],
      options: this.fb.array([], Validators.required)
    });
    if (this.receviedData) {
      const receivedOptions = this.receviedData.optionList || [];
      this.patchreceivedOption(receivedOptions);
    } else {
      this.addOption()

    }
  }

  createOptionGroupGroup(options: any): FormGroup {
    return this.fb.group({
      isCorrect: [options.isCorrect || '', Validators.required],
      optionText: [options.optionValue || '', Validators.required],
    });
  }

  patchreceivedOption(receivedOptions: any[]) {
    const productQuantityFormArray = this.questionForm.get('options') as FormArray;
    productQuantityFormArray.clear();

    receivedOptions.forEach((option: any) => {
      productQuantityFormArray.push(this.createOptionGroupGroup(option));
    });
  }

  addOption() {
    const productQuantity = this.questionForm.get('options') as FormArray;
    productQuantity.push(this.createOptionGroupGroup({}));
  }

  // Function to remove a quantity FormGroup from the productQuantity FormArray based on index
  removeOption(index: number) {
    const productQuantity = this.questionForm.get('options') as FormArray;
    productQuantity.removeAt(index);
  }

  get optionControls() {
    return (this.questionForm.get('options') as FormArray).controls;
  }


  onIsCorrectChange(index: number): void {
    const optionsArray = this.questionForm.get("options") as FormArray;
  
    // Iterate through all options
    for (let i = 0; i < optionsArray.length; i++) {
      if (i !== index) {
        // If the checkbox is not the one that triggered the change event, set it to false
        const optionGroup = optionsArray.at(i) as FormGroup;
        optionGroup.get("isCorrect")?.setValue(false);
      }
    }
  }

  getIsCorrectValue(index: number): boolean {
    const optionsArray = this.questionForm.get("options") as FormArray;
    const optionGroup = optionsArray.at(index) as FormGroup;
    return optionGroup.get("isCorrect")?.value;
  }


  changeTestId() {
    if (this.questionForm) {
      const selectedbsatExamTestId = this.questionForm.get("bsatExamTestId")?.value;
      console.log("Selected Category ID:", selectedbsatExamTestId);
    }
  }

  getAllQuestions() {
    this.httpService.get("bsatExamTest/getAllBsatExamTest").subscribe(
      (res) => {
        this.TestDetails = res.data;
        console.log("this.TestDetails", this.TestDetails);
      },
      (error: any) => {
        console.log("error", error);
      }
    );
  }

  createQuestion() {
    const payload = {
      bsatExamTestId: this.questionForm.get("bsatExamTestId")?.value,
      subject: this.questionForm.get('subject')?.value,
      question: this.questionForm.get("question")?.value,
      solution: this.questionForm.get("solution")?.value,
      // mulitpleCheck: this.questionForm.get("mulitpleCheck")?.value,
      // createAnother: this.questionForm.get("createAnother")?.value,
      optionList: this.questionForm.get("options")?.value.map((option: any) => {
        return {
          optionValue: option.optionText,
          // optionId: '', // You may need to generate unique IDs for each option
          isCorrect: option.isCorrect,
        };
      }),
    };

    this.httpService.post("bsatExamQuestion/createBsatExamQuestion", payload).subscribe({
      next: (response) => {
        this.TestDetails = response.data;
        console.log("this.TestDetails", this.TestDetails);
        this.router.navigate(["/admin-panel/bharat-sat/question-list"]);
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  updateQuestion() {
    const payload = {
      bsatExamTestId: this.questionForm.get("bsatExamTestId")?.value,
      // subject: this.questionForm.get('subject')?.value,
      question: this.questionForm.get("question")?.value,
      solution: this.questionForm.get("solution")?.value,
      optionList: this.questionForm.get("options")?.value.map((option: any) => {
        return {
          optionValue: option.optionText,
          // optionId: '', // You may need to generate unique IDs for each option
          isCorrect: option.isCorrect,
        };
      }),
    };

    this.httpService
      .put("bsatExamQuestion/updateBsatExamQuestion", this.receviedData._id, payload)
      .subscribe({
        next: (response) => {
          this.TestDetails = response.data;
          console.log("this.TestDetails", this.TestDetails);
          this.router.navigate(["/admin-panel/bharat-sat/question-list"]);
        },
        error: (err) => {
          console.log("err", err);
        },
        complete: () => {
          console.info("complete.!!");
        },
      });
  }

  clear() {
    this.questionForm.reset();
  }

}
