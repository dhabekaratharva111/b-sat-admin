import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import CommonConstants from 'src/app/shared/constants/global.const';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-test',
  templateUrl: './add-test.component.html',
  styleUrls: ['./add-test.component.scss']
})
export class AddTestComponent {
  classFrom: FormGroup;
  testDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  catImageFile: File | null = null;
  chapterImageSrc: string | null = null;
  selectedId: any;
  receviedData: any;
  chapterImg: any;
  isEditClicked: any = false
  base_url: any = environment.apiEndpoint;
  chapterDetails: any;
  classes = CommonConstants.CLASSES;
  medium = CommonConstants.MEDIUM;

  constructor(private httpService: HttpService, private fb: FormBuilder, private router: Router,) {
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if (this.receviedData) {
      if (this.receviedData.isEditClick) {
        this.isEditClicked = this.receviedData.isEditClick;
        // this.chapterImageSrc = this.receviedData.chapterImg;
        console.log('this.chapterImageSrc', this.chapterImageSrc)
      } else {
        this.isEditClicked = false,
          this.chapterImageSrc = this.receviedData.chapterImg;

      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {

  }
  expertForm() {
    this.classFrom = this.fb.group({
      classId: [this.receviedData ? this.receviedData.classId : '', Validators.required],
      medium: [this.receviedData ? this.receviedData.medium : '', Validators.required],
      bsatExamTestTimeDuration: [this.receviedData ? this.receviedData.bsatExamTestTimeDuration : '', Validators.required],
      bsatExamTestName: [this.receviedData ? this.receviedData.bsatExamTestName : '', Validators.required],
      bsatExamTestTimeDate: [this.receviedData ? this.convertToDatetimeLocalString(this.receviedData.bsatExamTestTimeDate) : '', Validators.required],
    });
  }




  convertToDatetimeLocalString(dateString: string): string {
    const date = new Date(dateString);
    const formatNumber = (num: number) => num.toString().padStart(2, '0');

    return `${date.getFullYear()}-${formatNumber(date.getMonth() + 1)}-${formatNumber(date.getDate())}T${formatNumber(date.getHours())}:${formatNumber(date.getMinutes())}`;
  }




  changeClassId() {
    if (this.classFrom) {
      const selectedclassId = this.classFrom.get('classId')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }

  changeMediumId(){
    if (this.classFrom) {
      const selectedclassId = this.classFrom.get('medium')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }

  // getAllSubject(){
  //   this.httpService.get('subject/getAllSubject').subscribe(
  //     (res) => {
  //       this.chapterDetails = res.data;
  //       console.log('this.chapterDetails', this.chapterDetails)
  //     },
  //     (error: any) => {
  //       console.log('error', error);
  //     }
  //   )
  // }


  createSubject() {
    this.httpService.post("bsatExamTest/createBsatExamTest", this.classFrom.value).subscribe({
      next: (response) => {
        this.testDetails = response.data;
        console.log('this.testDetails', this.testDetails);
        this.router.navigate(['/admin-panel/bharat-sat/test-list']);
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  updateSubject() {
    this.httpService.put("bsatExamTest/updateBsatExamTest", this.receviedData._id, this.classFrom.value).subscribe({
      next: (response) => {
        console.log('this.testDetails', this.testDetails);
        this.router.navigate(['/admin-panel/bharat-sat/test-list']);
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  clear() {
    this.classFrom.reset();
    this.chapterImageSrc = null; // Clear the image URL
  }
}
