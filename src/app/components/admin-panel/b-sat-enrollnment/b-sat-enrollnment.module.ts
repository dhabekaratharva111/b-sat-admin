import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BSatEnrollnmentRoutingModule } from './b-sat-enrollnment-routing.module';
import { QuestionListComponent } from './question-list/question-list.component';
import { BSatEnrollnmentComponent } from './enrollnment/b-sat-enrollnment.component';
import { AddQuestionComponent } from './add-question/add-question.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddTestComponent } from './add-test/add-test.component';
import { TestListComponent } from './test-list/test-list.component';
import { BsatExamResultComponent } from './bsat-exam-result/bsat-exam-result.component';
import { ScholarshipComponent } from './scholarship/scholarship.component';
import { AddScholarshipComponent } from './add-scholarship/add-scholarship.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    QuestionListComponent,
    BSatEnrollnmentComponent,
    AddQuestionComponent,
    AddTestComponent,
    TestListComponent,
    BsatExamResultComponent,
    ScholarshipComponent,
    AddScholarshipComponent,
  ],
  imports: [
    CommonModule,
    BSatEnrollnmentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class BSatEnrollnmentModule { }
