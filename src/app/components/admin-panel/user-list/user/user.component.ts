import { Component, QueryList, ViewChildren } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import CommonConstants from 'src/app/shared/constants/global.const';
import { ProductListDirective, SortEvent } from 'src/app/shared/directives/product-list.directive';
import { productList } from 'src/app/shared/interface/product-list';
import { CommonService } from 'src/app/shared/services/common.service';
import { ProductListService } from 'src/app/shared/services/ecommerce/product-list.service';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { PdfService } from 'src/app/shared/services/pdf.service';


const Swal = require('sweetalert2')
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {
  products$: Observable<productList[]>;
  total$: Observable<number>;

  // public PRODUCTLIST = data.PRODUCTLIST 

  @ViewChildren(ProductListDirective)
  headers!: QueryList<ProductListDirective>;
  response: any;
  userDetails: any=[];
  isEditClicked: boolean;
  studentFilter = {
    firstName: "",
    class: "",
    medium: "",
    page: 1,
    limit: 25,
  };
  classes = CommonConstants.CLASSES;
  medium = CommonConstants.MEDIUM;
  totalRecords: any = 0;
  studentDetails: any =[];



  constructor(public service: ProductListService, private router: Router, private httpService: HttpService, private commonService: CommonService,
    private toasterService: ToastrService,
    private loaderService : LoaderService,
    private pdfService: PdfService
  ) {
    this.products$ = service.support$;
    this.total$ = service.total$;

    this.response = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    console.log('this.qParams', this.response);

    this.getAllStudentCrud();
  }

  getAllStudentCrud() {
    this.loaderService.startLoader();
    this.httpService
      .post('studentRegistration/getAllStudentRegistration', this.studentFilter)
      .subscribe(
        (res) => {
          this.totalRecords = res.totalData
          this.userDetails = res.data;
          
          console.log('@@@@@@@@@this.studentDetails', this.userDetails);
          setTimeout(() => {
            this.loaderService.stopLoader();
        }, 200);
        },
        (error) => {
          setTimeout(() => {
            this.loaderService.stopLoader();
        }, 200);
        }
      );
  }

  downloadBalanceReportPDF() {
    this.pdfService.generateBalanceReportPDF(this.userDetails);
    this.toasterService.success("Download successfully..!");

  }

  deleteBibiyaneCategory(id: any) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "Once deleted, you will not be able to recover data! ",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then((result: any) => {
      if (result.value) {
        this.httpService.delete('bibiyaneSubCategory/deleteBibiyaneSubCat', id).subscribe(
          (res) => {
            this.getAllStudentCrud();
            swalWithBootstrapButtons.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
            this.toasterService.success("Deleted successfully..!");
          },
          (error) => {
            console.log('error', error);
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  editTheForm(item: any,) {
    this.isEditClicked = true;
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };
    this.router.navigate(['/admin-panel/user/add-user'], { state: objToSend });
  }

  onItemPerPageChange() {
    this.studentFilter.page = 1;
    this.getAllStudentCrud()
  }

  onSort({ column, direction }: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  totalPages(): number {
    return Math.ceil(this.totalRecords / this.studentFilter.limit);
  }

  getPages(): number[] {
    return Array(this.totalPages())
      .fill(0)
      .map((_, i) => i + 1);
  }

  onPageChanged(page: number) {
    this.studentFilter.page = page;
    this.getAllStudentCrud();
  }

  clearFilter(){
    this.studentFilter = {
      firstName: "",
      class: "",
      medium: "",
      page: 1,
      limit: 25,
    };
    this.getAllStudentCrud();
  }
}
