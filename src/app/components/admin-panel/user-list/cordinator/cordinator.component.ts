import { Component, QueryList, ViewChildren } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import CommonConstants from 'src/app/shared/constants/global.const';
import { ProductListDirective, SortEvent } from 'src/app/shared/directives/product-list.directive';
import { productList } from 'src/app/shared/interface/product-list';
import { CommonService } from 'src/app/shared/services/common.service';
import { ProductListService } from 'src/app/shared/services/ecommerce/product-list.service';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { PdfService } from 'src/app/shared/services/pdf.service';

const Swal = require('sweetalert2')
@Component({
  selector: 'app-cordinator',
  templateUrl: './cordinator.component.html',
  styleUrls: ['./cordinator.component.scss']
})
export class CordinatorComponent {
  products$: Observable<productList[]>;
  total$: Observable<number>;

  // public PRODUCTLIST = data.PRODUCTLIST 

  @ViewChildren(ProductListDirective)
  headers!: QueryList<ProductListDirective>;
  response: any;
  userDetails: any;
  isEditClicked: boolean;

  coordinatorFilter = {
    name: "",
    coordinatorCode:"",
    mobileNo:"",
    page: 1,
    limit: 25,
  };
  studentDetails: any;
  classes = CommonConstants.CLASSES;
  totalRecords: any = 0;


  constructor(public service: ProductListService ,private router: Router,
    private toasterService: ToastrService,
     private httpService: HttpService, private commonService: CommonService,
    private loaderService : LoaderService,
    private pdfService: PdfService
    ) {
    this.products$ = service.support$;
    this.total$ = service.total$;

    this.response = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    console.log('this.qParams', this.response);

    this.getAllCoordiantorCrud();
  }

  ngOnInit(){
    this.getAllCoordiantorCrud();
  }

getAllCoordiantorCrud() {
  this.loaderService.startLoader();
  this.httpService
    .post('registraition/getAllRegistraition', this.coordinatorFilter)
    .subscribe(
      (res) => {
        this.totalRecords = res.totalData
        this.userDetails = res.data;
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      },
      (error) => {
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      }
    );
}

onItemPerPageChange() {
  this.coordinatorFilter.page = 1;
  this.getAllCoordiantorCrud()
}

  editTheForm(item: any,) {
    this.isEditClicked=true;
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };
    this.router.navigate(['/admin-panel/user/edit-cordinator'], { state: objToSend });
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  totalPages(): number {
    return Math.ceil(this.totalRecords / this.coordinatorFilter.limit);
  }

  getPages(): number[] {
    return Array(this.totalPages())
      .fill(0)
      .map((_, i) => i + 1);
  }

  onPageChanged(page: number) {
    this.coordinatorFilter.page = page;
    this.getAllCoordiantorCrud();
  }

  downloadBalanceReportPDF() {
    this.pdfService.generateCoordiantorReport(this.userDetails);
    this.toasterService.success("Download successfully..!");

  }

  clearFilter(){
    this.coordinatorFilter = {
      name: "",
      coordinatorCode:"",
      mobileNo:"",
      page: 1,
      limit: 25,
    };
    this.getAllCoordiantorCrud();
  }
}
