import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserListRoutingModule } from './user-list-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { CordinatorComponent } from './cordinator/cordinator.component';
import { AddCordinatorComponent } from './add-cordinator/add-cordinator.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    UserComponent,
    AddUserComponent,
    CordinatorComponent,
    AddCordinatorComponent
  ],
  imports: [
    CommonModule,
    UserListRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ]
})
export class UserListModule { }
