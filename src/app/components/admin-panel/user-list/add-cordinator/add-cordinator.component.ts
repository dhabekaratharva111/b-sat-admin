import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-cordinator',
  templateUrl: './add-cordinator.component.html',
  styleUrls: ['./add-cordinator.component.scss']
})
export class AddCordinatorComponent {
  coordinatorForm: FormGroup;
  subjectDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  selectedId: any;
  receviedData: any;
  chapterImg: any;
  isEditClicked:any= false
  base_url: any = environment.apiEndpoint;
  chapterDetails : any;

  constructor( private httpService: HttpService,private fb: FormBuilder, private http: HttpClient,private router: Router,
    private toasterService: ToastrService,
    private loaderService : LoaderService,
    ){
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if(this.receviedData){
      if( this.receviedData.isEditClick){
        this.isEditClicked = this.receviedData.isEditClick ;
      }else{
         this.isEditClicked=true;
      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {
    
  }

  expertForm() {
    this.coordinatorForm = this.fb.group({
      name: [this.receviedData ? this.receviedData.name : '',  ],
      DOB: [this.receviedData ? this.receviedData.DOB : '',  ],
      age: [this.receviedData ? this.receviedData.age : '',  ],
      // coordinatorCode: [this.receviedData ? this.receviedData.coordinatorCode : '',  ],
      email: [this.receviedData ? this.receviedData.email : '',  ],
      mobileNo: [this.receviedData ? this.receviedData.mobileNo: '',  ],
      twelfthPass: [this.receviedData ? this.receviedData.twelfthPass : '',  ],
      });
  }

  
    
createSubject() {
  this.loaderService.startLoader();
    this.httpService.post("registraition/createRegistraition",this.coordinatorForm.value).subscribe({
      next: (response) => {
        console.log('this.subjectDetails', this.subjectDetails);
        this.router.navigate(['/admin-panel/user/cordinator']);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      this.toasterService.success("Data Added successfully..!");
      },
      error: (err) => {
        console.log("err", err);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
    }
  

    updateSubject() {
      this.loaderService.startLoader();
      this.httpService.put("registraition/updateRegistraition",this.receviedData._id,this.coordinatorForm.value).subscribe({
        next: (response) => {
          console.log('this.subjectDetails', this.subjectDetails);
          this.router.navigate(['/admin-panel/user/cordinator']);
          setTimeout(() => {
            this.loaderService.stopLoader();
        }, 200);
        this.toasterService.success("Data Updated successfully..!");

        },
        error: (err) => {
          console.log("err", err);
          setTimeout(() => {
            this.loaderService.stopLoader();
        }, 200);
        },
        complete: () => {
          console.info("complete.!!");
        },
      });
      }
    
    

  clear() {
    this.coordinatorForm.reset();
  }
}
