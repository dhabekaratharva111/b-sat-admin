import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCordinatorComponent } from './add-cordinator.component';

describe('AddCordinatorComponent', () => {
  let component: AddCordinatorComponent;
  let fixture: ComponentFixture<AddCordinatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCordinatorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddCordinatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
