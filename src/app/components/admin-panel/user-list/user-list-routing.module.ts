import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { CordinatorComponent } from './cordinator/cordinator.component';
import { AddCordinatorComponent } from './add-cordinator/add-cordinator.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'user-list',
        component: UserComponent,
        data: {
          title: "Student",
          breadcrumb: "Student",
        }
      },
      {
        path: 'add-user',
        component: AddUserComponent,
        data: {
          title: "Add Student",
          breadcrumb: "Add Student",
        }
      },
      {
        path: 'cordinator',
        component: CordinatorComponent,
        data: {
          title: "Cordinator",
          breadcrumb: "Cordinator",
        }
      },
      {
        path: 'edit-cordinator',
        component: AddCordinatorComponent,
        data: {
          title: "Add Cordinator",
          breadcrumb: "Add Cordinator",
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserListRoutingModule { }
