import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import CommonConstants from 'src/app/shared/constants/global.const';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent {
  UserForm: FormGroup;
  subjectDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  selectedId: any;
  receviedData: any;
  chapterImg: any;
  isEditClicked:boolean= false
  base_url: any = environment.apiEndpoint;
  chapterDetails : any;
  classes = CommonConstants.CLASSES;
  medium = CommonConstants.MEDIUM;

  constructor( private httpService: HttpService,private fb: FormBuilder, private http: HttpClient,private router: Router,
    private toasterService: ToastrService,
    private loaderService : LoaderService
    ){
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if(this.receviedData){
      if( this.receviedData.isEditClick){
        this.isEditClicked = this.receviedData.isEditClick ;
      }else{
         this.isEditClicked=true;
      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {
    
  }

  expertForm() {
    this.UserForm = this.fb.group({
      firstName: [this.receviedData ? this.receviedData.firstName :'', Validators.required],
      middleName: [this.receviedData ? this.receviedData.middleName :''],
      lastname: [this.receviedData ? this.receviedData.lastname :''],
      mobileNo: [this.receviedData ? this.receviedData.mobileNo :'', [Validators.required]],
      email: [this.receviedData ? this.receviedData.email :'', [Validators.required]],
      address1: [this.receviedData ? this.receviedData.address1 :'', Validators.required],
      address2: [this.receviedData ? this.receviedData.address2 :''],
      state: [this.receviedData ? this.receviedData.state :'', Validators.required],
      pincode: [this.receviedData ? this.receviedData.pincode :'', [Validators.required]],
      DOB: [this.receviedData ? this.receviedData.DOB :'', Validators.required],
      gender: [this.receviedData ? this.receviedData.gender :'', Validators.required],
      userId: [this.receviedData ? this.receviedData.userId :'', Validators.required],
      password: [this.receviedData ? this.receviedData.password :'', Validators.required],
      confirmpassword: [this.receviedData ? this.receviedData.confirmpassword :''],
      class: [this.receviedData ? this.receviedData.class :null, Validators.required],
      schoolName: [this.receviedData ? this.receviedData.schoolName :'', Validators.required],
      userType: [this.receviedData ? this.receviedData.userType :'student'],
      coordinatorCode: [this.receviedData ? this.receviedData.coordinatorCode :''],
      medium: [this.receviedData ? this.receviedData.medium : null, Validators.required],
      checkBox: [this.receviedData ? this.receviedData.checkBox :true],
    });
  }

  
  createSubject(){
    this.loaderService.startLoader();
    this.httpService.post("studentRegistration/createStudentRegistration",this.UserForm.value).subscribe({
      next: (response) => {
        console.log('this.subjectDetails', this.subjectDetails);
        this.router.navigate(['/admin-panel/user/user-list']);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      this.toasterService.success("Data Added successfully..!");

      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }
    

updateSubject() {
  this.loaderService.startLoader();
  this.httpService.put("studentRegistration/updateStudentRegistration",this.receviedData._id,this.UserForm.value).subscribe({
    next: (response) => {
      console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/user/user-list']);
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    this.toasterService.success("Data Updated successfully..!");

    },
    error: (err) => {
      console.log("err", err);
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    },
    complete: () => {
      console.info("complete.!!");
    },
  });
  }

  clear() {
    this.UserForm.reset();
  }
}
