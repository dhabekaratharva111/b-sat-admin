import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-chapter',
  templateUrl: './add-chapter.component.html',
  styleUrls: ['./add-chapter.component.scss']
})
export class AddChapterComponent {
  classFrom: FormGroup;
  subjectDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  catImageFile: File | null = null;
  chapterImageSrc: string | null = null;
  selectedId: any;
  receviedData: any;
  chapterImg: any;
  isEditClicked:any= false
  base_url: any = environment.apiEndpoint;
  chapterDetails : any;

  constructor( private httpService: HttpService,private fb: FormBuilder, private http: HttpClient,private router: Router,){
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if(this.receviedData){
      if( this.receviedData.isEditClick){
        this.isEditClicked = this.receviedData.isEditClick ;
        // this.chapterImageSrc = this.receviedData.chapterImg;
        console.log('this.chapterImageSrc', this.chapterImageSrc)
      }else{
         this.isEditClicked=false,
        this.chapterImageSrc = this.receviedData.chapterImg;

      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {
    this.getAllSubject();
  }
  expertForm() {
    this.classFrom = this.fb.group({
      subject: [this.receviedData ? this.receviedData.subject._id : '', Validators.required ],
      chaptername: [this.receviedData ? this.receviedData.chaptername : '', Validators.required ],
      chapterImg: [this.receviedData ? this.receviedData.chapterImg :'', Validators.required],
      });
  }

  changeClassId() {
    if (this.classFrom) {
      const selectedclassId = this.classFrom.get('classId')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }

  getAllSubject(){
    this.httpService.get('subject/getAllSubject').subscribe(
      (res) => {
        this.chapterDetails = res.data;
        console.log('this.chapterDetails', this.chapterDetails)
      },
      (error: any) => {
        console.log('error', error);
      }
    )
  }
  

  onFileSelected(event: any): void {
    this.chapterImageSrc = '';

    const formData: FormData = this.prepareFormData(event.target.files[0]);
    
    this.http.post<any>('https://bsat.onrender.com/uploadImage/createUploadImage', formData).subscribe(
      (response) => {
        console.log('Upload successful:', response);
                 this.chapterImageSrc = response.data.img
                 this.classFrom.get('chapterImg')?.patchValue(response.data.img)
      },
      (error) => {
        console.error('Upload error:', error);
      }
    );   
  }

  prepareFormData(chapterImg: File): FormData {
    const formData = new FormData();
    formData.append('img', chapterImg, chapterImg.name);
    return formData;    
  }

  

  createSubject(){                          
  this.httpService.post("chapter/createChapter",this.classFrom.value).subscribe({
    next: (response) => {
      this.subjectDetails = response.data;
      console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/chapter/chapter-list']);
    },
    error: (err) => {
      console.log("err", err);
    },
    complete: () => {
      console.info("complete.!!");
    },
  });
}

updateSubject() {
  this.httpService.put("chapter/updateChapter",this.receviedData._id,this.classFrom.value).subscribe({
    next: (response) => {
      console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/chapter/chapter-list']);
    },
    error: (err) => {
      console.log("err", err);
    },
    complete: () => {
      console.info("complete.!!");
    },
  });
  }

  clear() {
    this.classFrom.reset();
    this.chapterImageSrc = null; // Clear the image URL
  }
}
