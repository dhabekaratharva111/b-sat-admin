import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChapterListComponent } from './chapter-list/chapter-list.component';
import { AddChapterComponent } from './add-chapter/add-chapter.component';

var routingAnimation = localStorage.getItem('animate')
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'chapter-list',
        component: ChapterListComponent,
        data: {
          title: "Chapter list",
          breadcrumb: "Chapter-list",
          animation: [routingAnimation]
        }
      },
      {
        path: 'add-chapter',
        component: AddChapterComponent,
        data: {
          title: "Add Chapter",
          breadcrumb: "add-chapter",
          animation: [routingAnimation]
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChapterRoutingModule { }
