import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChapterRoutingModule } from './chapter-routing.module';
import { ChapterListComponent } from './chapter-list/chapter-list.component';
import { AddChapterComponent } from './add-chapter/add-chapter.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ChapterListComponent,
    AddChapterComponent
  ],
  imports: [
    CommonModule,
    ChapterRoutingModule,
    ReactiveFormsModule
  ]
})
export class ChapterModule { }
