import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { environment } from 'src/environments/environment';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import CommonConstants from 'src/app/shared/constants/global.const';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-study-material',
  templateUrl: './add-study-material.component.html',
  styleUrls: ['./add-study-material.component.scss']
})
export class AddStudyMaterialComponent {
  public Editor: any = ClassicEditor;
  studyMaterialFrom: FormGroup;
  subjectDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  catImageFile: File | null = null;
  chapterImageSrc: string | null = null;
  selectedId: any;
  receviedData: any;
  chapterImg: any;
  isEditClicked: any = false
  base_url: any = environment.apiEndpoint;
  chapterDetails: any;
  classes = CommonConstants.CLASSES;
  medium = CommonConstants.MEDIUM;

  constructor(private httpService: HttpService, private fb: FormBuilder,
    private toasterService: ToastrService,
     private http: HttpClient, private router: Router,
    private loaderService : LoaderService

    ) {
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if (this.receviedData) {
      if (this.receviedData.isEditClick) {
        this.isEditClicked = this.receviedData.isEditClick
      } else {
        this.isEditClicked = false;

      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {
    this.getAllStudyMaterial();
  }
  expertForm() {
    this.studyMaterialFrom = this.fb.group({
      classId: [this.receviedData ? this.receviedData.classId : null, Validators.required],
      medium: [this.receviedData ? this.receviedData.medium : null, Validators.required],
      studyMaterial: [this.receviedData ? this.receviedData.studyMaterial : '', Validators.required],
    });
  }

  changeClassId() {
    if (this.studyMaterialFrom) {
      const selectedclassId = this.studyMaterialFrom.get('classId')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }

  changeMediumId(){
    if (this.studyMaterialFrom) {
      const selectedmedium = this.studyMaterialFrom.get('medium')?.value;
      console.log('Selected Category ID:', selectedmedium);
    }
  }

  getAllStudyMaterial(){
    this.loaderService.startLoader();
    this.httpService.get('studyMaterial/getAllStudyMaterial').subscribe(
      (res) => {
        this.classes =  this.classes.map((classItem: { key: any; }) => {
          let isClassIdPresent = res.data.some((item:any) => item.classId === classItem.key);
          return {
            ...classItem,
            disabled: isClassIdPresent,
          };
        });
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      
      },
      (error: any) => {
        console.log('error', error);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      }
    )
  }

  createStudyMaterial() {
    this.loaderService.startLoader();
    this.httpService.post("studyMaterial/createStudyMaterial", this.studyMaterialFrom.value).subscribe({
      next: (response) => {
        this.subjectDetails = response.data;
        console.log('this.subjectDetails', this.subjectDetails);
        this.router.navigate(['/admin-panel/study-material/study-material']);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      },
      error: (err) => {
        console.log("err", err);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      this.toasterService.success("Data Added successfully..!");
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  updateStudyMaterial() {
    this.loaderService.startLoader();
    this.httpService.put("studyMaterial/updateStudyMaterial", this.receviedData._id, this.studyMaterialFrom.value).subscribe({
      next: (response) => {
        console.log('this.subjectDetails', this.subjectDetails);
        this.router.navigate(['/admin-panel/study-material/study-material']);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      this.toasterService.success("Data Updated successfully..!");
      },
      error: (err) => {
        console.log("err", err);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  clear() {
    this.studyMaterialFrom.reset();
    this.chapterImageSrc = null; // Clear the image URL
  }
}
