import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudyMaterialRoutingModule } from './study-material-routing.module';
import { StudyMaterialComponent } from './study-material/study-material.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddStudyMaterialComponent } from './add-study-material/add-study-material.component';
import { MockTestListComponent } from './mock-test-list/mock-test-list.component';
import { AddMockTestComponent } from './add-mock-test/add-mock-test.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PurchasedMaterialComponent } from './purchased-material/purchased-material.component';
import { AddPurchasedMaterialComponent } from './add-purchased-material/add-purchased-material.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    StudyMaterialComponent,
    AddStudyMaterialComponent,
    MockTestListComponent,
    AddMockTestComponent,
    PurchasedMaterialComponent,
    AddPurchasedMaterialComponent
  ],
  imports: [
    CommonModule,
    StudyMaterialRoutingModule,
    ReactiveFormsModule,
    CKEditorModule,
    SharedModule
  ]
})
export class StudyMaterialModule { }
