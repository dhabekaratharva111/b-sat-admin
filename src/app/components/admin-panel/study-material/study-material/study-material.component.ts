import { Component, QueryList, TemplateRef, ViewChildren } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { ProductListDirective, SortEvent } from 'src/app/shared/directives/product-list.directive';
import { productList } from 'src/app/shared/interface/product-list';
import { ProductListService } from 'src/app/shared/services/ecommerce/product-list.service';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';

const Swal = require('sweetalert2')
@Component({
  selector: 'app-study-material',
  templateUrl: './study-material.component.html',
  styleUrls: ['./study-material.component.scss']
})
export class StudyMaterialComponent {
  products$: Observable<productList[]>;
  total$: Observable<number>;

  // public PRODUCTLIST = data.PRODUCTLIST 

  @ViewChildren(ProductListDirective)
  headers!: QueryList<ProductListDirective>;
  response: any;
  subjectDetails: any;
  modalRef?: BsModalRef;
  template: TemplateRef<void>
  isModalOpen = false;
  selectedDataToView: any;

  constructor(public service: ProductListService ,private router: Router, private httpService: HttpService,private modalService: BsModalService,private loaderService : LoaderService) {
    this.products$ = service.support$;
    this.total$ = service.total$;

    this.response = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    console.log('this.qParams', this.response);

    this.getAllSubject();
  }

getAllSubject(){
  this.loaderService.startLoader();
  this.httpService.get('studyMaterial/getAllStudyMaterial').subscribe(
    (res) => {
      this.subjectDetails = res.data;
      console.log('this.subjectDetails', this.subjectDetails)
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    },
    (error) => {
      console.log('error', error);
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    }
  )
}
  


deleteChapter(id : any) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });
  
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "Once deleted, you will not be able to recover data! ",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then((result: any) => {
      if (result.value) {
        // If the user confirms the deletion
        this.httpService.delete('studyMaterial/deleteStudyMaterial',id).subscribe(
          (res) => {
            this.getAllSubject();
            swalWithBootstrapButtons.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
          },
          (error) => {
            console.log('error', error);
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // If the user cancels the deletion
      }
    });
  }

  editTheForm(item: any,) {
    item.isEditClick = true
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };
    this.router.navigate(['/admin-panel/study-material/add-study-material'], { state: objToSend });
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  // openModal(template: TemplateRef<void>) {
  //   this.modalRef = this.modalService.show(template);
  // }

  openModalToShowDetailsData(event: Event, item:any) {
    event.preventDefault();
    this.selectedDataToView = item?.studyMaterial;
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }

  getLimitedContent(content: string): string {
    const limit = 50; // Adjust the limit as needed
    if (content.length <= limit) {
      return content;
    }
    return content.substring(0, limit) + '...';
  }
}
