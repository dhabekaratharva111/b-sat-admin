import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasedMaterialComponent } from './purchased-material.component';

describe('PurchasedMaterialComponent', () => {
  let component: PurchasedMaterialComponent;
  let fixture: ComponentFixture<PurchasedMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchasedMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PurchasedMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
