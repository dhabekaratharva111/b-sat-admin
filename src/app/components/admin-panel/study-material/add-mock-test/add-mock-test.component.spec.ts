import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMockTestComponent } from './add-mock-test.component';

describe('AddMockTestComponent', () => {
  let component: AddMockTestComponent;
  let fixture: ComponentFixture<AddMockTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddMockTestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddMockTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
