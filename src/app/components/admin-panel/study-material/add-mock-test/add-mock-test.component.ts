import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import CommonConstants from 'src/app/shared/constants/global.const';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-mock-test',
  templateUrl: './add-mock-test.component.html',
  styleUrls: ['./add-mock-test.component.scss']
})
export class AddMockTestComponent {
  classFrom: FormGroup;
  subjectDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  catImageFile: File | null = null;
  chapterImageSrc: string | null = null;
  selectedId: any;
  receviedData: any;
  chapterImg: any;
  isEditClicked:any= false
  base_url: any = environment.apiEndpoint;
  chapterDetails : any;
  classes = CommonConstants.CLASSES;
  medium = CommonConstants.MEDIUM;

  constructor( private httpService: HttpService,private fb: FormBuilder, 
    private loaderService : LoaderService,
    private toasterService: ToastrService,
    private http: HttpClient,private router: Router,){
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if(this.receviedData){
      if( this.receviedData.isEditClick){
        this.isEditClicked = this.receviedData.isEditClick ;
        // this.chapterImageSrc = this.receviedData.chapterImg;
        console.log('this.chapterImageSrc', this.chapterImageSrc)
      }else{
         this.isEditClicked=false,
        this.chapterImageSrc = this.receviedData.chapterImg;

      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {
   
  }
  expertForm() {
    this.classFrom = this.fb.group({
      classId: [this.receviedData ? this.receviedData.classId : null, Validators.required ],
      medium: [this.receviedData ? this.receviedData.medium : null, Validators.required ],
      testTimeDuration: [this.receviedData ? this.receviedData.testTimeDuration : '', Validators.required ],
      testName: [this.receviedData ? this.receviedData.testName :'', Validators.required],
      });
  }

  changeClassId() {
    if (this.classFrom) {
      const selectedclassId = this.classFrom.get('classId')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }

  changemediumId() {
    if (this.classFrom) {
      const selectedclassId = this.classFrom.get('classId')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }
   

  createSubject(){     
    this.loaderService.startLoader();                     
  this.httpService.post("mockTest/createMockTest",this.classFrom.value).subscribe({
    next: (response) => {
      this.subjectDetails = response.data;
      console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/study-material/mock-test']);
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    this.toasterService.success("Data Added successfully..!");
    },
    error: (err) => {
      console.log("err", err);
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    },
    complete: () => {
      console.info("complete.!!");
    },
  });
}

updateSubject() {
  this.loaderService.startLoader();
  this.httpService.put("mockTest/updateMockTest",this.receviedData._id,this.classFrom.value).subscribe({
    next: (response) => {
      console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/study-material/mock-test']);
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    this.toasterService.success("Data Updated successfully..!");
    },
    error: (err) => {
      console.log("err", err);
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    },
    complete: () => {
      console.info("complete.!!");
      
    },
  });
  }

  clear() {
    this.classFrom.reset();
    this.chapterImageSrc = null; // Clear the image URL
  }
}
