import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPurchasedMaterialComponent } from './add-purchased-material.component';

describe('AddPurchasedMaterialComponent', () => {
  let component: AddPurchasedMaterialComponent;
  let fixture: ComponentFixture<AddPurchasedMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPurchasedMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddPurchasedMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
