import { Component, QueryList, ViewChildren } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { ProductListDirective, SortEvent } from 'src/app/shared/directives/product-list.directive';
import { productList } from 'src/app/shared/interface/product-list';
import { ProductListService } from 'src/app/shared/services/ecommerce/product-list.service';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';

const Swal = require('sweetalert2')
@Component({
  selector: 'app-mock-test-list',
  templateUrl: './mock-test-list.component.html',
  styleUrls: ['./mock-test-list.component.scss']
})
export class MockTestListComponent {
  products$: Observable<productList[]>;
  total$: Observable<number>;

  // public PRODUCTLIST = data.PRODUCTLIST 

  @ViewChildren(ProductListDirective)
  headers!: QueryList<ProductListDirective>;
  response: any;
  linkDetails: any;

  constructor(public service: ProductListService ,private router: Router, private httpService: HttpService,
    private loaderService : LoaderService, private toaster: ToastrService

    ) {
    this.products$ = service.support$;
    this.total$ = service.total$;

    this.response = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    console.log('this.qParams', this.response);

    this.getAllLinks();
  }

getAllLinks(){
  this.loaderService.startLoader();
  this.httpService.get('mockTest/getAllMockTest').subscribe(
    (res) => {
      this.linkDetails = res.data;
      console.log('this.linkDetails', this.linkDetails)
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    },
    (error) => {
      console.log('error', error);
      setTimeout(() => {
        this.loaderService.stopLoader();
    }, 200);
    }
  )
}
  
copyFunction(txt:string){
  navigator.clipboard.writeText(txt)
  .then(() => {
    console.log('Text copied to clipboard');
    this.toaster.success("Text copied to clipboard")
  })
  .catch((err) => {
    console.error('Unable to copy text to clipboard', err);
  })
}

deleteLink(id : any) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });
  
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "Once deleted, you will not be able to recover data! ",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then((result: any) => {
      if (result.value) {
        // If the user confirms the deletion
        this.httpService.delete('mockTest/deleteMockTest',id).subscribe(
          (res) => {
            this.getAllLinks();
            swalWithBootstrapButtons.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
          },
          (error) => {
            console.log('error', error);
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // If the user cancels the deletion
      }
    });
  }

  editTheForm(item: any,) {
    item.isEditClick = true
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };
    this.router.navigate(['/admin-panel/study-material/add-mock-test'], { state: objToSend });
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
