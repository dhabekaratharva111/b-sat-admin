import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudyMaterialComponent } from './study-material/study-material.component';
import { MockTestListComponent } from './mock-test-list/mock-test-list.component';
import { AddMockTestComponent } from './add-mock-test/add-mock-test.component';
import { AddStudyMaterialComponent } from './add-study-material/add-study-material.component';
import { AddPurchasedMaterialComponent } from './add-purchased-material/add-purchased-material.component';
import { PurchasedMaterialComponent } from './purchased-material/purchased-material.component';

var routingAnimation = localStorage.getItem('animate')
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'study-material',
        component: StudyMaterialComponent,
        data: {
          title: "Study Material List",
          breadcrumb: "Study Material List",
          animation: [routingAnimation]
        }
      },
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'add-study-material',
        component: AddStudyMaterialComponent,
        data: {
          title: "Study Material",
          breadcrumb: "Study Material",
          animation: [routingAnimation]
        }
      },
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'mock-test',
        component: MockTestListComponent,
        data: {
          title: "Test List",
          breadcrumb: "Test List",
          animation: [routingAnimation]
        }
      },
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'add-mock-test',
        component: AddMockTestComponent,
        data: {
          title: "Create Test",
          breadcrumb: "Create Test",
          animation: [routingAnimation]
        }
      },
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'add-purchased-material',
        component: AddPurchasedMaterialComponent,
        data: {
          title: "Add Purchased",
          breadcrumb: "add purchased",
          animation: [routingAnimation]
        }
      },
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'purchased-material',
        component: PurchasedMaterialComponent,
        data: {
          title: "Purchased Material",
          breadcrumb: "Purchased Material",
          animation: [routingAnimation]
        }
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudyMaterialRoutingModule { }
