import { HttpClient } from '@angular/common/http';
import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs';
import CommonConstants from 'src/app/shared/constants/global.const';
import { ProductListDirective, SortEvent } from 'src/app/shared/directives/product-list.directive';
import { productList } from 'src/app/shared/interface/product-list';
import { CommonService } from 'src/app/shared/services/common.service';
import { ProductListService } from 'src/app/shared/services/ecommerce/product-list.service';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import * as XLSX from 'xlsx';

const Swal = require('sweetalert2')
@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent {
  mockQuestionForm: FormGroup;
  products$: Observable<productList[]>;
  total$: Observable<number>;

  // public PRODUCTLIST = data.PRODUCTLIST 

  @ViewChildren(ProductListDirective)
  headers!: QueryList<ProductListDirective>;
  response: any;
  QuestionDetails: any[];

  alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
  TestDetails: any;
  mockTestQuestions: any;
  productImageSrc: string;
  optionImages: any;
  classes = CommonConstants.CLASSES;
  medium = CommonConstants.MEDIUM;
  selectedClass: any =null;
  selectedMedium: any ='english';
  selectedMockTest: any = null;


  constructor(public service: ProductListService, private commonService: CommonService,
    private router: Router, private httpService: HttpService,
    private loaderService: LoaderService, private http: HttpClient
  ) {
    this.products$ = service.support$;
    this.total$ = service.total$;

    this.response = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    console.log('this.qParams', this.response);

    // this.GetAllQuestion();
    // this.getAllMockTest()
  }

  loadMockTestByClassAndMedium() {
    this.loaderService.startLoader();
    this.httpService.getById(`mockTest/getAllMockTestByIds/${this.selectedClass}`,this.selectedMedium).subscribe(
      (res) => {
        console.log('00000000000000000000res', res)
        if(res.data.length>0){
          this.TestDetails = res.data;
        }else{
          this.mockTestQuestions =[];
          this.TestDetails=[]
        }

        // this.QuestionDetails = res.data;
        // console.log('this.QuestionDetails', this.QuestionDetails);
        setTimeout(() => {
          this.loaderService.stopLoader();
        }, 200);
      },
      (error) => {
        console.log('error', error);
        setTimeout(() => {
          this.loaderService.stopLoader();
        }, 200);
      }
    )
  }


  // GetAllQuestion(){
  //   this.loaderService.startLoader();
  //   this.httpService.get('mockTestQuestions/getAllmockTestQuestion').subscribe(
  //     (res) => {
  //       this.QuestionDetails = res.data;
  //       console.log('this.QuestionDetails', this.QuestionDetails);
  //       setTimeout(() => {
  //         this.loaderService.stopLoader();
  //     }, 200);
  //     },
  //     (error) => {
  //       console.log('error', error);
  //       setTimeout(() => {
  //         this.loaderService.stopLoader();
  //     }, 200);
  //     }
  //   )
  // }

  getAllMockTest() {
    this.loaderService.startLoader();
    this.httpService.get("mockTest/getAllMockTest").subscribe(
      (res) => {
        this.TestDetails = res.data;
        if (this.TestDetails) {
          this.selectedMockTest = this.TestDetails[0]._id;
          // this.loadMockTestQuestionByTestId()
        }
        setTimeout(() => {
          this.loaderService.stopLoader();
        }, 200);
        console.log("this.TestDetails", this.TestDetails);
      },
      (error: any) => {
        console.log("error", error);
        setTimeout(() => {
          this.loaderService.stopLoader();
        }, 200);
      }
    );
  }

  loadMockTestQuestionByTestId() {
    this.loaderService.startLoader();

    this.httpService.getById("mockTestQuestions/getMockTestQuestionByMockTestId", this.selectedMockTest).subscribe(
      (res) => {
        this.mockTestQuestions = res.data;
        console.log("this.mockTestQuestions", this.mockTestQuestions);
        setTimeout(() => {
          this.loaderService.stopLoader();
        }, 200);
      },
      (error: any) => {
        console.log("error", error);
        setTimeout(() => {
          this.loaderService.stopLoader();
        }, 200);
      }
    );
  }
  // selectedMockTest(arg0: string, selectedMockTest: any) {
  //   throw new Error('Method not implemented.');
  // }

  prepareFormData(productImg: File): FormData {
    const formData = new FormData();
    formData.append('img', productImg, productImg.name);
    return formData;
  }

  onFileSelected(id: any, event: any): void {
    event.stopPropagation()
    this.productImageSrc = '';
    const formData: FormData = this.prepareFormData(event.target.files[0]);

    this.http.post<any>('https://bsat.onrender.com/uploadImage/createUploadImage', formData).subscribe(
      (response) => {
        console.log('Upload successful:', response);
        this.productImageSrc = response.data.img;
        this.editImage(id, this.productImageSrc)
      },
      (error) => {
        console.error('Upload error:', error);
      }
    );
  }

  editImage(id: any, img: any) {
    this.httpService
      .put("mockTestQuestions/updatemockTestQuestion", id, { questionImg: img })
      .subscribe(
        (res) => {
          this.getAllMockTest(); // Update the data after the status is changed
          // this.toastr.success('Image Uploaded !!');
        },
        (error) => {
          console.log("Error updating myCrops status:", error);
        }
      );
  }

  deleteQuestion(id: any) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "Once deleted, you will not be able to recover data! ",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then((result: any) => {
      if (result.value) {
        // If the user confirms the deletion
        this.httpService.delete('mockTestQuestions/deletemockTestQuestion', id).subscribe(
          (res) => {
            this.selectedMockTest = this.TestDetails[0]._id;

            // this.loadMockTestQuestionByTestId()
            swalWithBootstrapButtons.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
          },
          (error) => {
            console.log('error', error);
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // If the user cancels the deletion
      }
    });
  }

  editTheForm(item: any, event: any) {
    event.stopPropagation()
    item.isEditClick = true;
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };
    this.router.navigate(['/admin-panel/mock-test/add-question'], { state: objToSend });
  }

  bulkUpload(evt: any) {
    this.loaderService.startLoader();

    const target: DataTransfer = <DataTransfer>evt.target;
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      const jsonData: any[] = XLSX.utils.sheet_to_json(ws, { header: 1 });

      // Assuming the first row contains headers
      const headers: string[] = jsonData[0];

      let data = jsonData.slice(1).map((row: any) =>
        headers.reduce((acc: any, header, index) => {
          acc[header] = row[index];
          return acc;
        }, {})
      );

      this.httpService.post('mockTestQuestions/bulkUpload', data).subscribe(
        (res) => {
          evt.target.value = '';
          this.selectedMockTest = this.TestDetails[0]._id;
          // this.loadMockTestQuestionByTestId()
          setTimeout(() => {
            this.loaderService.stopLoader();
          }, 200);
        },
        (error) => {

        }
      );
    };

    reader.readAsBinaryString(target.files[0]);
  }




  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
