import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormArray,
  FormControl,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { HttpService } from "src/app/shared/services/http/http.service";
import { LoaderService } from "src/app/shared/services/loader.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-add-question",
  templateUrl: "./add-question.component.html",
  styleUrls: ["./add-question.component.scss"],
})
export class AddQuestionComponent {
  questionForm: FormGroup;
  TestDetails: any;
  receviedData: any;
  isEditClicked: boolean = false;
  questionImg: string;

  constructor(
    private httpService: HttpService,
    private fb: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private loaderService : LoaderService
  ) {
    this.receviedData =
      this.router.getCurrentNavigation()?.extras.state?.["queryParams"];
    if (this.receviedData) {
      if (this.receviedData.isEditClick) {
        this.isEditClicked = this.receviedData.isEditClick;
      } else {
        this.isEditClicked = false;
      }
    }
    console.log("this.receviedData", this.receviedData);
  }

  ngOnInit() {
    this.expertForm();
    this.getAllMockTest();
  }

  expertForm() {
    this.questionForm = this.fb.group({
      mockTestId: [
        this.receviedData ? this.receviedData.mockTestId._id : "",
        Validators.required,
      ],
      question: [
        this.receviedData ? this.receviedData.question : "",
        Validators.required,
      ],
      questionImg: [
        this.receviedData ? this.receviedData.questionImg : "",
      ],
      options: this.fb.array([], Validators.required),
    });
    if (this.receviedData) {
      const receivedOptions = this.receviedData.optionList || [];
      this.patchreceivedOption(receivedOptions);
    } else {
      this.addOption()

    }
  }

  createOptionGroupGroup(options: any): FormGroup {
    return this.fb.group({
      isCorrect: [options.isCorrect || '', Validators.required],
      optionText: [options.optionValue || '', Validators.required],
    });
  }

  patchreceivedOption(receivedOptions: any[]) {
    const productQuantityFormArray = this.questionForm.get('options') as FormArray;
    productQuantityFormArray.clear();

    receivedOptions.forEach((option: any) => {
      productQuantityFormArray.push(this.createOptionGroupGroup(option));
    });
  }

  addOption() {
    const productQuantity = this.questionForm.get('options') as FormArray;
    productQuantity.push(this.createOptionGroupGroup({}));
  }

  // Function to remove a quantity FormGroup from the productQuantity FormArray based on index
  removeOption(index: number) {
    const productQuantity = this.questionForm.get('options') as FormArray;
    productQuantity.removeAt(index);
  }

  get optionControls() {
    return (this.questionForm.get('options') as FormArray).controls;
  }


  onIsCorrectChange(index: number): void {
    const optionsArray = this.questionForm.get("options") as FormArray;
  
    // Iterate through all options
    for (let i = 0; i < optionsArray.length; i++) {
      if (i !== index) {
        // If the checkbox is not the one that triggered the change event, set it to false
        const optionGroup = optionsArray.at(i) as FormGroup;
        optionGroup.get("isCorrect")?.setValue(false);
      }
    }
  }

  getIsCorrectValue(index: number): boolean {
    const optionsArray = this.questionForm.get("options") as FormArray;
    const optionGroup = optionsArray.at(index) as FormGroup;
    return optionGroup.get("isCorrect")?.value;
  }


  changeMockTestId() {
    if (this.questionForm) {
      const selectedmockTestId = this.questionForm.get("mockTestId")?.value;
      console.log("Selected Category ID:", selectedmockTestId);
    }
  }

  getAllMockTest() {
    this.httpService.get("mockTest/getAllMockTest").subscribe(
      (res) => {
        this.TestDetails = res.data;
        console.log("this.TestDetails", this.TestDetails);
      },
      (error: any) => {
        console.log("error", error);
      }
    );
  }

  private prepareFormData(questionImg: File): FormData {
    const formData = new FormData();
    formData.append('img', questionImg, questionImg.name);
    return formData;
  }


  onFileSelectQuestionImage(event: any): void {
    this.questionImg = '';

    const formData: FormData = this.prepareFormData(event.target.files[0]);

    this.http.post<any>('https://bsat.onrender.com/uploadImage/createUploadImage', formData).subscribe(
      (response) => {
        console.log('Upload successful:', response);
        this.questionImg = response.data.img
        this.questionForm.get('questionImg')?.patchValue(response.data.img)
      },
      (error) => {
        console.error('Upload error:', error);
      }
    );
  }

  createQuestion() {
    this.loaderService.startLoader();
    const payload = {
      mockTestId: this.questionForm.get("mockTestId")?.value,
      subject: this.questionForm.get('subject')?.value,
      question: this.questionForm.get("question")?.value,
      solution: this.questionForm.get("solution")?.value,
      questionImg: this.questionForm.get("questionImg")?.value,
      // mulitpleCheck: this.questionForm.get("mulitpleCheck")?.value,
      // createAnother: this.questionForm.get("createAnother")?.value,
      optionList: this.questionForm.get("options")?.value.map((option: any) => {
        return {
          optionValue: option.optionText,
          // optionId: '', // You may need to generate unique IDs for each option
          isCorrect: option.isCorrect,
        };
      }),
    };

    this.httpService.post("mockTestQuestions/createmockTestQuestion", payload).subscribe({
      next: (response) => {
        this.TestDetails = response.data;
        console.log("this.TestDetails", this.TestDetails);
        this.router.navigate(["/admin-panel/mock-test/question-list"]);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      },
      error: (err) => {
        console.log("err", err);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  updateQuestion() {
    this.loaderService.startLoader();
    const payload = {
      mockTestId: this.questionForm.get("mockTestId")?.value,
      subject: this.questionForm.get('subject')?.value,
      question: this.questionForm.get("question")?.value,
      solution: this.questionForm.get("solution")?.value,
      questionImg: this.questionForm.get("questionImg")?.value,
      optionList: this.questionForm.get("options")?.value.map((option: any) => {
        return {
          optionValue: option.optionText,
          optionId: option.optionId, // You may need to generate unique IDs for each option
          isCorrect: option.isCorrect,
        };
      }),
    };

    this.httpService
      .put("mockTestQuestions/updatemockTestQuestion", this.receviedData._id, payload)
      .subscribe({
        next: (response) => {
          this.TestDetails = response.data;
          console.log("this.TestDetails", this.TestDetails);
          this.router.navigate(["/admin-panel/mock-test/question-list"]);
          setTimeout(() => {
            this.loaderService.stopLoader();
        }, 200);
        },
        error: (err) => {
          console.log("err", err);
          setTimeout(() => {
            this.loaderService.stopLoader();
        }, 200);
        },
        complete: () => {
          console.info("complete.!!");
        },
      });
  }

  clear() {
    this.questionForm.reset();
  }
}
