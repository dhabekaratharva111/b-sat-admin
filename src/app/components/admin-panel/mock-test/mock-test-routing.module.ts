import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionListComponent } from './question-list/question-list.component';
import { AddQuestionComponent } from './add-question/add-question.component';

var routingAnimation = localStorage.getItem('animate')

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'question-list',
        component: QuestionListComponent,
        data: {
          title: "Question list",
          breadcrumb: "Question-List",
          animation: [routingAnimation]
        }
      },
      {
        path: 'add-question',
        component: AddQuestionComponent,
        data: {
          title: "Add Question",
          breadcrumb: "add-question",
          animation: [routingAnimation]
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MockTestRoutingModule { }
