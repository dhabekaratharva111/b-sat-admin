import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MockTestRoutingModule } from './mock-test-routing.module';
import { QuestionListComponent } from './question-list/question-list.component';
import { AddQuestionComponent } from './add-question/add-question.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    QuestionListComponent,
    AddQuestionComponent,
    
  ],
  imports: [
    CommonModule,
    MockTestRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ]
})
export class MockTestModule { }
