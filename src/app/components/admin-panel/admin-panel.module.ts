import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminPanelRoutingModule } from './admin-panel-routing.module';

@NgModule({
  declarations: [
    // BSatEnrollnmentComponent,
  ],
  imports: [
    CommonModule,
    AdminPanelRoutingModule,
  ]
})
export class AdminPanelModule { }
