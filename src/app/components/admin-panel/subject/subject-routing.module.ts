import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubjectListComponent } from './subject-list/subject-list.component';
import { AddSubjectComponent } from './add-subject/add-subject.component';

var routingAnimation = localStorage.getItem('animate')

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'subject-list',
        component: SubjectListComponent,
        data: {
          title: "Subject List",
          breadcrumb: "Subject-List",
          animation: [routingAnimation]
        }
      },
      {
        path: 'add-subject',
        component: AddSubjectComponent,
        data: {
          title: "Add Subject",
          breadcrumb: "add-subject",
          animation: [routingAnimation]
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubjectRoutingModule { }
