import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-subject',
  templateUrl: './add-subject.component.html',
  styleUrls: ['./add-subject.component.scss']
})
export class AddSubjectComponent {
classFrom: FormGroup;
  subjectDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  subjectImageSrc: string | null = null;
  selectedId: any;
  receviedData: any;
  subjectImg: any;
  isEditClicked:any= false
  base_url: any = environment.apiEndpoint;

  constructor( private httpService: HttpService,private fb: FormBuilder, private http: HttpClient,private router: Router,){
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if(this.receviedData){
      if( this.receviedData.isEditClick){
        this.isEditClicked = this.receviedData.isEditClick ;
        this.subjectImageSrc = this.receviedData.subjectImg;
        console.log('this.subjectImageSrc', this.subjectImageSrc)
      }else{
         this.isEditClicked=false,
        this.subjectImageSrc = this.receviedData.subjectImg;

      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {
  }
  expertForm() {
    this.classFrom = this.fb.group({
      classId: [this.receviedData ? this.receviedData.classId : '',Validators.required ],
      subjectName: [this.receviedData ? this.receviedData.subjectName : '',Validators.required ],
      subjectImg: [this.receviedData ? this.receviedData.subjectImg :'',Validators.required ],
      });
  }

  changeClassId() {
    if (this.classFrom) {
      const selectedclassId = this.classFrom.get('classId')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }
  

  onFileSelected(event: any): void {
    this.subjectImageSrc = '';

    const formData: FormData = this.prepareFormData(event.target.files[0]);
    
    this.http.post<any>('https://bsat.onrender.com/uploadImage/createUploadImage', formData).subscribe(
      (response) => {
        console.log('Upload successful:', response);
                 this.subjectImageSrc = response.data.img
                 this.classFrom.get('subjectImg')?.patchValue(response.data.img)
      },
      (error) => {
        console.error('Upload error:', error);
      }
    );   
  }

  prepareFormData(subjectImg: File): FormData {
    const formData = new FormData();
    formData.append('img', subjectImg, subjectImg.name);
    return formData;    
  }

  

  createSubject(){                          
  this.httpService.post("subject/createSubject",this.classFrom.value).subscribe({
    next: (response) => {
      this.subjectDetails = response.data;
      console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/subject/subject-list']);
    },
    error: (err) => {
      console.log("err", err);
    },
    complete: () => {
      console.info("complete.!!");
    },
  });
}

updateSubject() {
  this.httpService.put("subject/updateSubject",this.receviedData._id,this.classFrom.value).subscribe({
    next: (response) => {
      console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/subject/subject-list']);
    },
    error: (err) => {
      console.log("err", err);
    },
    complete: () => {
      console.info("complete.!!");
    },
  });
  }

  clear() {
    this.classFrom.reset();
    this.subjectImageSrc = null; // Clear the image URL
  }
}