import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ELearningRoutingModule } from './e-learning-routing.module';
import { AddElearningComponent } from './add-elearning/add-elearning.component';
import { ElearningListComponent } from './elearning-list/elearning-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    AddElearningComponent,
    ElearningListComponent
  ],
  imports: [
    CommonModule,
    ELearningRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class ELearningModule { }
