import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddElearningComponent } from './add-elearning.component';

describe('AddElearningComponent', () => {
  let component: AddElearningComponent;
  let fixture: ComponentFixture<AddElearningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddElearningComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddElearningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
