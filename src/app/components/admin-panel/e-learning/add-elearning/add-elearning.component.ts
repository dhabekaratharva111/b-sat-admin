import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import CommonConstants from 'src/app/shared/constants/global.const';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-elearning',
  templateUrl: './add-elearning.component.html',
  styleUrls: ['./add-elearning.component.scss']
})
export class AddElearningComponent {
  linkFrom: FormGroup;
  // subjectDetails: any;
  selectedclassId: any;
  classId: any;
  catImg: any;
  uploadedImageUrl: Object;
  catImageFile: File | null = null;
  chapterImageSrc: string | null = null;
  selectedId: any;
  receviedData: any;
  chapterImg: any;
  isEditClicked:any= false
  base_url: any = environment.apiEndpoint;
  chapterDetails : any;
  classes = CommonConstants.CLASSES;
  medium = CommonConstants.MEDIUM;

  constructor( private httpService: HttpService,private fb: FormBuilder, private http: HttpClient,private router: Router,){
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    if(this.receviedData){
      if( this.receviedData.isEditClick){
        this.isEditClicked = this.receviedData.isEditClick ;
        // this.chapterImageSrc = this.receviedData.chapterImg;
        console.log('this.chapterImageSrc', this.chapterImageSrc)
      }else{
         this.isEditClicked=false,
        this.chapterImageSrc = this.receviedData.chapterImg;

      }
    }
    console.log('this.receviedData', this.receviedData)
    this.expertForm();

  }

  ngOnInit(): void {
  }
  expertForm() {
    this.linkFrom = this.fb.group({
      classId: [this.receviedData ? this.receviedData.classId : null, Validators.required],
      medium: [this.receviedData ? this.receviedData.medium : null, Validators.required],
      videoName: [this.receviedData ? this.receviedData.videoName :'', Validators.required],
      videoLink: [this.receviedData ? this.receviedData.videoLink :'',Validators.required ],
      });
  }

  // changeChapterId() {
  //   if (this.linkFrom) {
  //     const selectedchapterId = this.linkFrom.get('chapterId')?.value;
  //     console.log('Selected Category ID:', selectedchapterId);
  //   }
  // }

  changeClassId() {
    if (this.linkFrom) {
      const selectedclassId = this.linkFrom.get('classId')?.value;
      console.log('Selected Category ID:', selectedclassId);
    }
  }

  changeMediumId(){
    if (this.linkFrom) {
      const selectedmedium = this.linkFrom.get('medium')?.value;
      console.log('Selected Category ID:', selectedmedium);
    }
  }


  createLink(){                          
  this.httpService.post("addElerning/createAddElerning",this.linkFrom.value).subscribe({
    next: (response) => {
      // this.subjectDetails = response.data;
      // console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/e-learning/e-learning-list']);
    },
    error: (err) => {
      console.log("err", err);
    },
    complete: () => {
      console.info("complete.!!");
    },
  });
}

updateLink() {
  this.httpService.put("addElerning/updateAddElerning",this.receviedData._id,this.linkFrom.value).subscribe({
    next: (response) => {
      // console.log('this.subjectDetails', this.subjectDetails);
      this.router.navigate(['/admin-panel/e-learning/e-learning-list']);
    },
    error: (err) => {
      console.log("err", err);
    },
    complete: () => {
      console.info("complete.!!");
    },
  });
  }

  clear() {
    this.linkFrom.reset();
    this.chapterImageSrc = null; // Clear the image URL
  }
}
