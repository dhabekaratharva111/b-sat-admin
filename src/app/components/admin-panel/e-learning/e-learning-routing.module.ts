import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ElearningListComponent } from './elearning-list/elearning-list.component';
import { AddElearningComponent } from './add-elearning/add-elearning.component';

var routingAnimation = localStorage.getItem('animate')
const routes: Routes = [
  {
    path: 'e-learning-list',
    component: ElearningListComponent,
    data: {
      title: "E Learning",
      breadcrumb: "E Learning",
      animation: [routingAnimation]
    }
  },
  {
    path: 'add-e-learning',
    component: AddElearningComponent,
    data: {
      title: "Add Elearning",
      breadcrumb: "Add Elearning",
      animation: [routingAnimation]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ELearningRoutingModule { }
