import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BSatEnrollnmentComponent } from './b-sat-enrollnment/enrollnment/b-sat-enrollnment.component';
import { UserListModule } from '../../components/admin-panel/user-list/user-list.module';

var routingAnimation = localStorage.getItem('animate')

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'user',
        data: {
          title: "User List",
          breadcrumb: "User List"
        },
        loadChildren: () => import('../../components/admin-panel/user-list/user-list.module').then(m => m.UserListModule),
      },
      {
        path: 'subject',
        data: {
          title: "Subject List",
          breadcrumb: "Subject"
        },
        loadChildren: () => import('../../components/admin-panel/subject/subject.module').then(m => m.SubjectModule),
      },
      {
        path: 'chapter',
        data: {
          title: "Chapter List",
          breadcrumb: "chapter"
        },
        loadChildren: () => import('../../components/admin-panel/chapter/chapter.module').then(m => m.ChapterModule),
      },
      {
        path: 'mock-test',
        data: {
          title: "Questions",
          breadcrumb: "Questions"
        },
        loadChildren: () => import('../../components/admin-panel/mock-test/mock-test.module').then(m => m.MockTestModule),
      },
    {
      path: 'e-learning',
      data: {
        title: "E learning",
        breadcrumb: "E learning"
      },
      loadChildren: () => import('../../components/admin-panel/e-learning/e-learning.module').then(m => m.ELearningModule),
    },
    {
      path: 'bharat-sat',
      data: {
        title: "Bharat Sat",
        breadcrumb: "Bharat Sat"
      },
      loadChildren: () => import('../../components/admin-panel/b-sat-enrollnment/b-sat-enrollnment.module').then(m => m.BSatEnrollnmentModule),
    },
      // {
      //   path: 'b-sat-enrollnment',
      //   component: BSatEnrollnmentComponent,
      //   data: {
      //     title: "Bharat Sat Enrollnment",
      //     breadcrumb: "Bharat Sat Enrollnment",
      //     animation: [routingAnimation]
      //   }
      // },
      {
        path: 'study-material',
        data: {
          title: "Study material",
          breadcrumb: "Study material"
        },
        loadChildren: () => import('../../components/admin-panel/study-material/study-material.module').then(m => m.StudyMaterialModule),
      },
      {
        path: 'study-material',
        data: {
          title: "Study material",
          breadcrumb: "Study material"
        },
        loadChildren: () => import('../../components/admin-panel/study-material/study-material.module').then(m => m.StudyMaterialModule),
      },
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPanelRoutingModule { }
