import { Component, ElementRef, OnInit, TemplateRef } from '@angular/core';
import { NavService } from 'src/app/shared/services/nav/nav.service';
import { AuthenticationService } from '../../services/authentication.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http/http.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  studentForm: FormGroup;
  modalRef?: BsModalRef;
  collapseSidebar: boolean = true;
  uploadedImagesUrls: string[] = [];
  uploadedImageUrl: any;
  uploadedImageUrl1: string | null = null;
  productImageSrc: any;
  classFee: any;
  classes = [
    { key: '1', value: 'Class 1' },
    { key: '2', value: 'Class 2' },
    { key: '3', value: 'Class 3' },
    { key: '4', value: 'Class 4' },
    { key: '5', value: 'Class 5' },
    { key: '6', value: 'Class 6' },
    { key: '7', value: 'Class 7' },
    { key: '8', value: 'Class 8' },
    { key: '8', value: 'Class 8' },
    { key: '9', value: 'Class 9' },
    { key: '10', value: 'Class 10' },
    { key: '11', value: 'Class 11' },
    { key: '12', value: 'Class 12' },
 
  ];
  constructor(private navServices: NavService, private http : HttpClient,
    private elementRef: ElementRef, private authenticationService: AuthenticationService,private modalService: BsModalService,private fb: FormBuilder, private httpService: HttpService) { }
  open = false;

  sidebarToggle() {
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
  }
  logoutFunc() {
    // this.authService.SignOut();
  }
  ngOnInit(): void {
    this.studentForm = this.fb.group({
      className: ['', Validators.required],
      examFee: [{ value: '', disabled: true }, Validators.required],
      studentPhoto: [null, Validators.required],
      signaturePhoto: [null, Validators.required],
      
    });
  }
  // menu open
  openMenu(){
    this.open = !this.open
  }

  //logout
  logout(){
    this.authenticationService.logout()
  }

  languageToggle() {
    this.navServices.language = !this.navServices.language;
  }
  openModal(template: TemplateRef<void>) {
    this.modalRef = this.modalService.show(template);
  }

  uploadProductImages(event: any): void {
    const files = event.target.files;
  
    if (files && files.length > 0) {
      const formData = new FormData();
  
      // Append each file to the FormData
      for (const file of files) {
        formData.append('img', file, file.name);
      }
  
      // Send the FormData to the multiple upload image API
      this.http.post('https://bsat.onrender.com/uploadImage/createUploadImage', formData)
        .subscribe(
          (response: any) => {
            console.log('Upload successful:', response);
  
            this.uploadedImagesUrls = response['img'];
  
            this.productImageSrc = this.uploadedImagesUrls[0];
  
            if (this.uploadedImagesUrls.length > 1) {
              this.studentForm.patchValue({
                signaturePhoto: this.uploadedImagesUrls[1]
              });
            }
          },
          (error) => {
            console.error('Upload error:', error);
          }
        );
    }
  }



  
  // this.http.post('https://bsat.onrender.com/uploadImage/createUploadImage', formData)

loadSubjectsForClass(selectedClassId: string) {
  this.httpService.getById('registrationForScholership/getRegistrationForScholershipByClass', selectedClassId).subscribe(
    (response) => {
      this.classFee = response.data;
      console.log('@@@@', this.classFee);

      // Assuming 'examFee' is a property in the 'classFee' response
      const examFeeValue = this.classFee.examFee;

      // Concatenate the currency symbol (Rupees) with the 'examFee' value
      const formattedExamFee = `₹ ${examFeeValue}`;

      // Set the formatted value for the 'examFee' form control
      this.studentForm.get('examFee')?.setValue(formattedExamFee);
    },
    (error) => {
      console.error('API Error:', error);
    }
  );
}

  

  submitForm(){

  }
}
