import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  private loader: HTMLHeadingElement | null = null;
  private isLoading: boolean = false;

  constructor() { }


  startLoader(): void {
    this.isLoading = true;
  }

  stopLoader(): void {
    this.isLoading = false;
  }

  getStatus(): boolean {
    return this.isLoading;
  }
 }
