
// pdf.service.ts

import { Injectable } from '@angular/core';
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable';
import html2canvas from 'html2canvas';
import { HttpService } from './http/http.service';

@Injectable({
  providedIn: 'root',
})
export class PdfService {
  private customFontUrl = 'assets/fonts/Shivaji01.ttf'; // Update the path to your font file
  paymentHistory: any;
  currentDate = new Date();
  formattedDate = this.formatDate(this.currentDate);
  formattedTime = this.formatTime(this.currentDate);
  fileHeadName = `${this.formattedDate}-${this.formattedTime}`;

  constructor(private httpService: HttpService) {
  }

  generateBalanceReportPDF(data: any[]) {
    const doc = new jsPDF({
      orientation: 'landscape'
    });

    doc.setFont('arial');

    autoTable(doc, {
      body: [
        [
          {
            content: 'Learn And Achieve - User',
            styles: {
              halign: 'left',
              fontSize: 18,
              textColor: '#24695c'
            }
          },
          {
            content: `Report - ${this.formattedDate}`,
            styles: {
              halign: 'right',
              fontSize: 18,
              textColor: '#24695c'
            }
          }
        ],
      ],
      theme: 'plain',
      styles: {
        fillColor: '#ffffff'
      }
    });

    autoTable(doc, {
      head: [['Name', 'Mobile', 'Email', 'DOB', 'Gender', 'School', 'Class/Medium']],
      body: this.bindUserValue(data),
      headStyles: {
        fillColor: '#24695c'
      }
    });

    return doc.save(`Report_${this.fileHeadName}.pdf`);
  }

  generateCoordiantorReport(data: any[]) {
    const doc = new jsPDF({
      orientation: 'landscape'
    });

    doc.setFont('arial');

    autoTable(doc, {
      body: [
        [
          {
            content: 'Learn And Achieve - Coordinator',
            styles: {
              halign: 'left',
              fontSize: 16,
              textColor: '#24695c'
            }
          },
          {
            content: `Report - ${this.formattedDate}`,
            styles: {
              halign: 'right',
              fontSize: 18,
              textColor: '#24695c'
            }
          }
        ],
      ],
      theme: 'plain',
      styles: {
        fillColor: '#ffffff'
      }
    });

    autoTable(doc, {
      head: [['Name', 'DOB','Age','Email','Mobile No','Coordinator Code']],
      body: this.bindValue(data),
      headStyles: {
        fillColor: '#24695c'
      }
    });

    return doc.save(`Report_${this.fileHeadName}.pdf`);
  }

  bindUserValue(data:any) {
    console.log('data', data)
    let formatedData:any = [];
    data.forEach((element:any) => {
      formatedData.push([element.firstName+''+element.middleName+''+element.lastname, element.mobileNo,element.email, element.DOB,
      element.gender,
      , element.schoolName,element.class])
    });
    return formatedData;
  }


  bindValue(data:any) {
    console.log('data', data)
    let formatedData:any = [];
    data.forEach((element:any) => {
      formatedData.push([element.name, element.DOB,element.age, element.email
      , element.mobileNo,element.coordinatorCode])
    });
    return formatedData;
  }

  formatDateForPDF(dateString: string): string {
    // Check if dateString is a valid date string
    if (!dateString || isNaN(Date.parse(dateString))) {
      return ''; // or handle the error in an appropriate way
    }

    const date = new Date(dateString);
    const options: Intl.DateTimeFormatOptions = { day: '2-digit', month: '2-digit', year: 'numeric' };
    return new Intl.DateTimeFormat('en-GB', options).format(date);
  }

  private formatDate(date: Date): string {
    const day = ('0' + date.getDate()).slice(-2) as string;
    const month = ('0' + (date.getMonth() + 1)).slice(-2) as string;
    const year = date.getFullYear().toString();
    return `${day}-${month}-${year}`;
  }

  private formatTime(date: Date): string {
    const hours = ('0' + (date.getHours() % 12) || 12)
      .toString()
      .slice(-2) as string;
    const minutes = ('0' + date.getMinutes()).slice(-2) as string;
    const ampm = date.getHours() >= 12 ? 'PM' : 'AM';
    return `${hours}-${minutes}-${ampm}`;
  }
}
