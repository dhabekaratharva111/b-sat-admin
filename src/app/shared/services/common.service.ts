import { Injectable } from "@angular/core";
import { AppConfigConstants } from "../constants/global.const";

@Injectable({
  providedIn: "root",
})
export class CommonService {
  public appConfig = AppConfigConstants.APP_CONFIG;
  showSpinner : any;
  constructor() { }

}
