import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { AuthenticationService } from "../services/authentication.service";

@Injectable({
  providedIn: "root",
})
export class AutoLoginGuard implements CanActivate {
  constructor(private authService: AuthenticationService, private router: Router) {}

  canActivate(): boolean {
    this.authService.isAuthenticated.subscribe((authenticated) => {
      if (authenticated) {
        this.router.navigate(["/admin-panel/user/user-list"]);
        return false;
      } else {
        return true;
      }
    });
    return true;
  }
}
