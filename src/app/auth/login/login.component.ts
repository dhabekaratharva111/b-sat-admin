import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavigationExtras, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import CommonConstants from "src/app/shared/constants/global.const";
import { AuthenticationService } from "src/app/shared/services/authentication.service";
import { LoaderService } from "src/app/shared/services/loader.service";
import { StorageService } from "src/app/shared/services/storage.service";
import { TokenService } from "src/app/shared/services/token.service";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {

    public show: boolean = false;
    public loginForm: FormGroup;
    public errorMessage: any;

    constructor(private fb: FormBuilder, public router: Router,
        private authenticationService: AuthenticationService,
        private toastr: ToastrService,
        private storageService: StorageService,
        private toasterService: ToastrService,
        private tokenService: TokenService,
        private loaderService : LoaderService) {
        this.loginForm = this.fb.group({
            userId: ["", [Validators.required, Validators.email]],
            password: ["", Validators.required],
        });
    }

    ngOnInit() { }

    showPassword() {
        this.show = !this.show;
    }


    login(): void {
        this.loaderService.startLoader();
        this.authenticationService.login(this.loginForm.value.userId, this.loginForm.value.password).subscribe({
            next: (response) => {
                this.router.navigate(["/admin-panel/user/user-list"]);
                this.tokenService.setToken(response.token);
                setTimeout(() => {
                    this.loaderService.stopLoader();
                }, 200);
                const sendData = {
                    userId: this.loginForm.value.userId,
                    password: this.loginForm.value.password,
                    from: 'login'
                };
                this.authenticationService.isAuthenticated.next(true);
                this.toasterService.success("Login successfully..!");
            },
            error: (err) => {
                console.log("err", err);
                setTimeout(() => {
                    this.loaderService.stopLoader();
                    // Check the error response and display an alert message accordingly
                    if (err && err.error && err.error.message) {
                        // Display the custom error message
                        this.toasterService.error("Invalid Credentials...!");
                    } else {
                        // Display a generic error message
                    }
                }, 200);
            },
            complete: () => {
                console.info("complete.!!");
            },
        });
    }
    

}